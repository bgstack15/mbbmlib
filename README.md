# Readme for mbbmlib
This project exists to programmatically export my bookmarks from the profiles of Mozilla-based web browsers to html, without having to run the browser. Also, I wanted to have icons displayed in the outputted html.

## `mmbmlib` upstream
This project is the upstream: [https://gitlab.com/bgstack15/mbbmlib.git](https://gitlab.com/bgstack15/mbbmlib.git)

## Alternatives
Just use the built-in export bookmarks feature.

## Dependencies
* python3

## How to use mbbmlib
The most basic way to use this library is to run `export_bookmarks_to_html`.

    mbbmlib.export_bookmarks_to_html("/home/bgstack15/.mozilla/firefox/12345678.default", "/mnt/public/Public/bookmarks.html", debuglev=8, fetch_icons=True, iconsize=32)

See [ffbookmarkexporter.py](ffbookmarkexporter.py) for more examples.

## References
connman-gtk-xdg-autostart/README.md

## Differences from upstream
None
