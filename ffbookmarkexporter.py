#!/usr/bin/env python3
# This is more of a demo script than a useful thing on its own.

import mbbmlib

# Export a mozilla profile's bookmarks to html with favicons
mbbmlib.export_bookmarks_to_html("/home/bgstack15/.mozilla/firefox/12345678.default", "/mnt/public/Public/bookmarks.html", debuglev=8, fetch_icons=True)

# Get a nested list of bookmarks from that profile. The only additional use it has right now is to pretty-print which is worse than the html.
bm = mbbmlib.Bookmark("root") # bootstrap bookmark object
bm.load_from_places("places.sqlite", limit=2000, fetch_icons=False, iconsize=32, debuglev=8)
bm = bm.unflatten()
bm.pretty_print()
